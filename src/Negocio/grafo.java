package Negocio;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class grafo {

private ArrayList<Set<Integer>> _vecinos; // conjunto de grafoDirigidos = cantidad de vecinos

public grafo()
{
_vecinos = new ArrayList<Set<Integer>>();

}

public int vertices()
{
return _vecinos.size();
}

// Manejo de aristas
public void agregarArista(int origen, int destino)
{
verificarArista(origen, destino);

_vecinos.get(origen).add(destino);
_vecinos.get(destino).add(origen);
}

public void agregarVertice ()
{
_vecinos.add(new HashSet<Integer>());
}

public void agregarVertice (int destino)
{
_vecinos.add(new HashSet<Integer>());
agregarArista(destino , vertices()-1);	
}

public void eliminarArista(int origen, int destino)
{
verificarArista(origen, destino);

_vecinos.get(origen).remove(destino);
_vecinos.get(destino).remove(origen);
}

public boolean existeArista(int origen, int destino)
{
verificarArista(origen, destino);
return _vecinos.get(origen).contains(destino);
}

// Grado de un v�rtice
public int grado(int i)
{
verificarVertice(i);

return _vecinos.get(i).size();
}

// Vecinos de un v�rtice
public Set<Integer> vecinos(int i)
{
verificarVertice(i);

return _vecinos.get(i);
}

// Determina si el conjunto de v�rtices es una clique
public boolean esClique(Set<Integer> conjunto)
{
if( conjunto == null )
throw new IllegalArgumentException("El conjunto no puede ser null!");

for(Integer i: conjunto)
verificarVertice(i);

for(Integer i: conjunto)
for(Integer j: conjunto) if( i != j )
{
if( existeArista(i,j) == false )
return false;
}

return true;
}

// C�digo defensivo
private void verificarArista(int i, int j)
{
verificarVertice(i);
verificarVertice(j);
//	verificarDistintos(i, j);
}

private void verificarVertice(int i)
{
if( i < 0 || i >= vertices() )
throw new IllegalArgumentException("El v�rtice " + i + "no existe!");
}

public static void main(String[] args) {
// TODO Auto-generated method stub

grafo g = new grafo();
System.out.println(g.vertices());
g.agregarVertice();
System.out.println(g.vertices());
g.agregarVertice();
g.agregarArista(0, 1);
//g.agregarArista(0, 2);
//	g.agregarArista(2, 1);
//System.out.println("--------------");
System.out.println(g.vecinos(0));
//System.out.println(g.vecinos(1));


}
}