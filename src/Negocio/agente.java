package Negocio;

import java.util.ArrayList;
import java.util.Set;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class agente {
	
	private String nombre;
	private int id;
	private Coordinate coordenada;
	private String contacto;
	
	public agente(String Nombre, int ID, Coordinate Coordenada, String Contacto)
	{
		nombre = Nombre;
		id = ID;
		coordenada = Coordenada;
		contacto = Contacto;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public void setNombre(String Nombre)
	{
		nombre = Nombre ;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int ID)
	{
		id = ID ;
	}
	
	public Coordinate getCoordenada()
	{
		return coordenada;
	}
	
	public void setCoordenada(Coordinate Coordenada)
	{
		coordenada = Coordenada ;
	}

	
	public String getContacto()
	{
		return contacto;
	}
	
	public void setContacto(String Contacto)
	{
		contacto = Contacto ;
	}
}
